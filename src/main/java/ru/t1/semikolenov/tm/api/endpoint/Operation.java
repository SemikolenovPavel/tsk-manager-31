package ru.t1.semikolenov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.AbstractRequest;
import ru.t1.semikolenov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(@NotNull RQ request);

}

