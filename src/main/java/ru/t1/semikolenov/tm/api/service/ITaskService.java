package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task>{

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    Task updateById(
            @NotNull String userId,
            @NotNull String id, String name,
            @NotNull String description
    );

    Task updateById(
            @NotNull String userId,
            @NotNull String id, String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    Task updateByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    Task updateByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    Task changeStatusById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull Status status
    );

    Task changeStatusByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

}
