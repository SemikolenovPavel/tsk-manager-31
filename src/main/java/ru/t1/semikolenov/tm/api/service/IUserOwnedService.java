package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.api.repository.IUserOwnedRepository;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {



}
