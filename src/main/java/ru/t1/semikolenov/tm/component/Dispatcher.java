package ru.t1.semikolenov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.endpoint.Operation;
import ru.t1.semikolenov.tm.dto.request.AbstractRequest;
import ru.t1.semikolenov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @Nullable
    public Object call(@NotNull final AbstractRequest request) {
        @NotNull final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}

