package ru.t1.semikolenov.tm.exception.field;

public final class EmptyRoleException extends AbstractFieldException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}
