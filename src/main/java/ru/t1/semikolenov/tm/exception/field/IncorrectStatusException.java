package ru.t1.semikolenov.tm.exception.field;

public final class IncorrectStatusException extends AbstractFieldException {

    public IncorrectStatusException(final String message) {
        super("Error! Status is incorrect! Status `" + message + "` not supported...");
    }

}
