package ru.t1.semikolenov.tm.exception.user;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class IncorrectLoginOrPasswordException extends AbstractException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}
