package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.ICommandRepository;
import ru.t1.semikolenov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class CommandRepository implements ICommandRepository {

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        final String name = command.getName();
        if (!name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        if (argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return mapByArgument.values();
    }

}
