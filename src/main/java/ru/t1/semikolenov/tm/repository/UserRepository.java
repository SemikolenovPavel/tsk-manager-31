package ru.t1.semikolenov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.IUserRepository;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.model.User;
import ru.t1.semikolenov.tm.util.HashUtil;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

}
