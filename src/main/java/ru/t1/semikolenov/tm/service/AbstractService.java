package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.IRepository;
import ru.t1.semikolenov.tm.api.service.IService;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.exception.entity.ModelNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.IncorrectIndexException;
import ru.t1.semikolenov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(@NotNull R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        return repository.add(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) {
        @Nullable Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        @Nullable Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IncorrectIndexException();
        if (index >= repository.getSize()) throw new IncorrectIndexException();
        return repository.removeByIndex(index);
    }

    @Override
    public void removeAll(@NotNull Collection<M> collection) {
        if (collection.isEmpty()) return;
        repository.removeAll(collection);
    }

}
