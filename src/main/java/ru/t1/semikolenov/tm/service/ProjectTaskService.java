package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.repository.ITaskRepository;
import ru.t1.semikolenov.tm.api.service.IProjectTaskService;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull IProjectRepository projectRepository,
            @NotNull ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (    taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.findAllByProjectId(userId, projectId)
                .forEach(task -> taskRepository.removeById(task.getId()));
        Optional.ofNullable(projectRepository.removeById(projectId)).
                orElseThrow(ProjectNotFoundException::new);
    }

}
